// imports from npm
const express = require('express');
const { body, validationResult } = require('express-validator');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const cors = require('cors');
const rand = require('random-key');
const bodyParser = require('body-parser');

// imports from /src
const accounts = require('./accounts/accounts');
const packages = require('./packages/packages');

// app definitions
const app = express();

// variables
const port = process.env['JPK_PORT'] || 3000;
const key = rand.generate();

// configurations
app.set('views', './views');
app.set('view engine', 'pug');
app.set('domain', 'jpk.junickim.me');

const navbar = {
	'Log in': '/login',
	Register: '/register',
};

// middleware (cookies)
//app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use(
	session({
		secret: key,
		duration: 30 * 60 * 1000,
		resave: false,
		saveUninitialized: true,
	})
);

// custom middleware

// check if user has correct permissions.
const Permissions = (req, res, next) => {
	// adminstrator always receives permissions.
	if (req.session.username !== req.params.user || !req.session.username) {
		let params = { navbar: navbar };
		if (req.session.username) {
			params = {
				...params,
				username: req.session.username,
			};
		}
		res.render('403.pug', params);
	} else {
		next();
	}
};

const User_exists = async (req, res, next) => {
	if (!(await accounts.username_exists(req.params.user))) {
		let params = { navbar: navbar };
		if (req.session.username) {
			params = {
				...params,
				username: req.session.username,
			};
		}
		res.render('404.pug', params);
	} else {
		next();
	}
};

const Build_exists = async (req, res, next) => {
	if (!(await packages.exists(req.params.user, req.params.package))) {
		let params = { navbar: navbar };
		if (req.session.username) {
			params = {
				...params,
				username: req.session.username,
			};
		}
		res.render('404.pug', params);
	} else {
		next();
	}
};

// callbacks

app.get('/', async (req, res) => {
	let params = { navbar: navbar, users: await accounts.all_users() };
	if (req.session.username) {
		params.username = req.session.username;
	}
	res.render('index.pug', params);
});

// log in page
app.get('/login', function (req, res) {
	if (req.session.username) {
		res.render('login.pug', {
			navbar: navbar,
			success: true,
			username: req.session.username,
		});
	} else {
		res.render('login.pug', {
			navbar: navbar,
		});
	}
});

// handle log in form
app.post('/login', async function (req, res) {
	const name = req.body.name;
	const password = req.body.password;
	if (!(await accounts.auth(name, password))) {
		res.status(400).render('login.pug', {
			navbar: navbar,
			fail: true,
		});
	} else {
		const un = await accounts.get_username(name);
		req.session.username = un;
		// send a redirect to /users/username
		res.redirect('/users/' + un);
	}
});

// sign up page
app.get('/register', async function (req, res) {
	if (req.session.username) {
		res.render('register.pug', {
			navbar: navbar,
			username: req.session.username,
		});
	} else {
		res.render('register.pug', {
			navbar: navbar,
		});
	}
});

// handle sign up form
app.post(
	'/register',
	body('email').isEmail(),
	body('username').isLength({ min: 5 }),
	body('password').isLength({ min: 7 }),
	async function (req, res) {
		// if there are errors
		if (!validationResult(req).isEmpty()) {
			res.status(400).render('register.pug', {
				navbar: navbar,
				fail: `
                Invalid input. Emails must be
                valid, usernames must be at least 5
                characters, and passwords must be
                at least 7 characters.`,
			});
		} else if (req.body.password !== req.body.repeat) {
			res.status(400).render('register.pug', {
				navbar: navbar,
				fail: 'Passwords do not match',
			});
		} else if (
			!(await accounts.signup(
				req.body.username,
				req.body.email,
				req.body.password
			))
		) {
			res.status(400).render('register.pug', {
				navbar: navbar,
				fail: 'Username/Email exists or DB error.',
			});
		} else {
			res.render('register.pug', {
				navbar: navbar,
				success: true,
			});
		}
	}
);

// home page
app.get('/users/:user', User_exists, async function (req, res) {
	let params = {
		navbar: navbar,
		user: req.params.user,
		packages: await packages.list(req.params.user),
	};
	// modify to hold packages.
	if (req.session.username) {
		params.username = req.session.username;
		if (req.session.username === req.params.user) {
			params.permissions = true;
		}
	}
	res.render('home.pug', params);
});

// authenticate before deleting account
app.get('/users/:user/delete', User_exists, Permissions, async (req, res) => {
	const params = {
		navbar: navbar,
		username: req.session.username,
		path: `/users/${req.params.user}/deleteaccount`,
	};
	res.render('auth.pug', params);
});

// delete account
app.post(
	'/users/:user/deleteaccount',
	User_exists,
	Permissions,
	async (req, res) => {
		if (!accounts.auth(req.session.username, req.body.password)) {
			res.send(`Account deletion failed. (invalid password)`);
			return;
		}
		// delete account (and all packages).
		await accounts.delete_account(req.session.username);
		await packages.delete_account(req.session.username);
		const username = req.session.username;
		delete req.session.username;
		res.redirect('/');
	}
);

// expose form for editing a package
app.get(
	'/users/:user/:package/edit',
	User_exists,
	Build_exists,
	Permissions,
	async function (req, res) {
		const x = await packages.info(req.params.user, req.params.package);
		if (x.length !== 0) {
			let b = x[0];
			b = {
				owner: req.params.user,
				name: req.params.package,
				info: b.info,
				dependencies: b.dependencies.join(' '),
				upstream: b.upstream,
			};
			const path = `/users/${req.params.user}/${req.params.package}/edit`;
			let params = { navbar: navbar, package: b, path: path };
			if (req.session.username) {
				params = {
					...params,
					username: req.session.username,
				};
			}

			res.render('edit.pug', params);
		} else {
			res.status(500).send('db error.');
		}
	}
);

// edit package
app.post(
	'/users/:user/:package/edit',
	User_exists,
	Build_exists,
	Permissions,
	async function (req, res) {
		const path = `/users/${req.params.user}/${req.params.package}/edit`;
		const x = await packages.info(req.params.user, req.params.package);
		const upd = {
			owner: req.params.user,
			name: req.params.package,
			dependencies: req.body.dependencies.split(/\s+/),
			upstream: req.body.upstream,
			info: req.body.info,
		};
		let params = { navbar: navbar, path: path };
		if (!(await packages.edit(upd))) {
			res.status(500).send('Update Error.');
			return;
		} else {
			upd.dependencies = upd.dependencies.join(' ');
			params.package = upd;
			params.success = true;
		}

		if (req.session.username) {
			params.username = req.session.username;
		}
		res.render('edit.pug', params);
	}
);

app.get('/users/:user/add', User_exists, Permissions, async (req, res) => {
	let path = `/users/${req.params.user}/add`;
	let params = { navbar: navbar, path: path, username: req.session.username };
	res.render('add.pug', params);
});

app.post('/users/:user/add', User_exists, Permissions, async (req, res) => {
	let path = `/users/${req.params.user}/add`;
	let params = { navbar: navbar, path: path, username: req.session.username };
	const name = req.body.name;
	if (RegExp('\\s+|/').test(name)) {
		params.fail = `Name '${name}' contains whitespace or '/'`;
	} else if (name === 'add' || name === 'delete' || name === 'deleteaccount') {
		params.fail = `Name '${name}' is reserved.`;
	} else if (name.length === 0) {
		params.fail = `Name '${name}' has no characters.`;
	} else if (!(await packages.add(req.params.user, name))) {
		params.fail = `Name '${name}' is already taken.`;
	}
	if (params.fail) {
		res.status(400).render('add.pug', params);
	} else {
		params.success = name;
		res.render('add.pug', params);
	}
});

app.get(
	'/users/:user/:package/json',
	User_exists,
	Build_exists,
	async (req, res) => {
		const d = (await packages.info(req.params.user, req.params.package))[0];
		res.json({
			owner: d.owner,
			name: d.name,
			dependencies: d.dependencies,
			upstream: d.upstream,
			info: d.info,
		});
	}
);

// authenticate before deleting account

app.get(
	'/users/:user/:package/delete',
	User_exists,
	Permissions,
	async (req, res) => {
		const params = {
			navbar: navbar,
			username: req.session.username,
			path: `/users/${req.params.user}/${req.params.package}/delete`,
		};
		res.render('authpkg.pug', params);
	}
);

app.post(
	'/users/:user/:package/delete',
	User_exists,
	Build_exists,
	Permissions,
	async (req, res) => {
		if (!accounts.auth(req.session.username, req.body.password)) {
			res.send(`Account deletion failed. (invalid password)`);
			return;
		}
		await packages.remove(req.params.user, req.params.package);
		const path = `/users/${req.params.user}`;
		res.redirect(path);
	}
);

app.get('*', (req, res) => {
	let params = { navbar: navbar };
	if (req.session.username) {
		params = {
			...params,
			username: req.session.username,
		};
	}
	res.render('404.pug', params);
});
// execution

console.log(`JPK running on port ${port}`);
app.listen(port);
