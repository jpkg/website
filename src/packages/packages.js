// Account implementations/encryptions

const mongoose = require('mongoose');
const emailValidator = require('email-validator');

// establish connections

mongoose.connect('mongodb://localhost:27017/JPK', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'error connecting to the db.'));
db.on('open', () => {
	console.log('Packages DB opened.');
});

// Schema/DB

// owner, name of build, dependencies, and info about package.

const Schema = new mongoose.Schema({
	owner: String,
	name: String,
	dependencies: [String],
	upstream: String,
	info: String,
});

const Packages = mongoose.model('Packages', Schema);

// add builds to database.
// errors will occur if names have a '/'.
const add = async (owner, name) => {
	const re = new RegExp('/|\\s');
	if (re.test(name)) {
		return false;
	}
	try {
		const result = await Packages.find({ owner: owner, name: name });
		if (result.length != 0) {
			return false;
		}
		const b = new Packages({
			owner: owner,
			name: name,
			dependencies: [],
			info: '',
			upstream: '',
		});
		b.save();
		return true;
	} catch (e) {
		console.error(e);
		return false;
	}
};

// add builds to database.
// errors will occur if names have a '/' or whitespace.
const edit = async (obj) => {
	try {
		const b = new Packages(obj);
		await Packages.deleteMany({ owner: obj.owner, name: obj.name });
		b.save();
		return true;
	} catch (e) {
		console.error(e);
		return false;
	}
};

const remove = async (owner, name) => {
	try {
		const result = await Packages.find({ owner: owner, name: name });
		if (result.length == 0) {
			return false;
		}
		await Packages.deleteMany({ owner: owner, name: name });
		return true;
	} catch (e) {
		console.error(e);
		return false;
	}
};

// lists builds by owner.
const list = async (owner) => {
	try {
		return await Packages.find({ owner: owner });
	} catch (e) {
		return [];
	}
};

const info = async (owner, name) => {
	try {
		return await Packages.find({ owner: owner, name: name });
	} catch (e) {
		console.error(e);
		return null;
	}
};

const exists = async (owner, name) => {
	try {
		return (await Packages.find({ owner: owner, name: name })).length !== 0;
	} catch (e) {
		console.error(e);
		return false;
	}
};

const delete_account = async (owner) => {
	try {
		await Packages.deleteMany({ owner: owner });
	} catch (e) {
		console.error(e);
	}
};

const clear = async () => {
	await Packages.deleteMany({});
};

module.exports = {
	list: list,
	remove: remove,
	info: info,
	add: add,
	edit: edit,
	exists: exists,
	delete_account: delete_account,
};

// tests
const run = async () => {
	console.log(await Packages.find({}));
	//console.log(await add("juni", "first"));
	//await clear();
};

//clear();
//run();
