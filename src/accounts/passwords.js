// Authentication/Passwords with bcrypt
const bcrypt = require('bcrypt');

const rounds = process.env['BCRYPT_ROUNDS'] || 11;

const generate = async (password) => {
	try {
		return await bcrypt.hash(password, rounds);
	} catch (e) {
		console.log(e);
	}
	return null;
};

const authenticate = async (password, hash) => {
	try {
		return await bcrypt.compare(password, hash);
	} catch (e) {
		console.log(e);
	}
	return false;
};

module.exports = {
	generate: generate,
	authenticate: authenticate,
};
