// Account implementations/encryptions

const rand = require('random-key');
const mongoose = require('mongoose');
const email_validator = require('email-validator');
const { generate, authenticate } = require('./passwords');

// establish connections

mongoose.connect('mongodb://localhost:27017/JPK', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'error connecting to the db.'));
db.on('open', () => {
	console.log('Accounts DB opened.');
});

// Schema/DB

const Schema = new mongoose.Schema({
	//
	username: String,
	email: String,
	hash: String,
});

const Account = mongoose.model('Accounts', Schema);

// functions

const email_exists = async (email) => {
	try {
		const result = await Account.find({ email: email });
		return result && result.length != 0;
	} catch (e) {
		console.error(e);
	}
	return false;
};

const username_exists = async (username) => {
	try {
		const result = await Account.find({ username: username });
		return result && result.length != 0;
	} catch (e) {
		console.error(e);
	}
};

const signup = async (username, email, password) => {
	if ((await email_exists(email)) || (await username_exists(username))) {
		return false;
	}
	// '/' cannot be included in a username, nor can whitespace.
	const re = new RegExp('/|\\s');
	if (re.test(email) || re.test(username)) {
		return false;
	}
	try {
		const hash = await generate(password);
		const acc = new Account({
			username: username,
			email: email,
			hash: hash,
		});
		await acc.save();
		return true;
	} catch (e) {
		console.log(e);
		return false;
	}
};

const get_username = async (name) => {
	try {
		if (email_validator.validate(name)) {
			// user submitted an email.
			return (await Account.findOne({ email: name }).exec()).username;
		} else {
			return name;
		}
	} catch (e) {
		console.error(e);
		return null;
	}
};

const delete_account = async (name) => {
	try {
		await Account.deleteMany({ username: name });
	} catch (e) {
		console.error(e);
	}
};

const auth = async (name, password) => {
	try {
		if (email_validator.validate(name)) {
			// user submitted an email.
			const result = await Account.findOne({ email: name }).exec();
			return await authenticate(password, result.hash);
		} else {
			const result = await Account.findOne({ username: name }).exec();
			return await authenticate(password, result.hash);
		}
	} catch (e) {
		return false;
	}
};

const add_build = async (name, build) => {
	try {
		const result = await Account.findOne({ username: name }).exec();
		if (!result.builds.includes(build)) {
			result.builds.push(build);
			await result.save();
			return true;
		}
		return false;
	} catch (e) {
		return false;
	}
};

const remove_build = async (name, build) => {
	try {
		const result = await Account.findOne({ username: name }).exec();
		if (result.builds.includes(build)) {
			result.builds = result.builds.filter((b) => b !== build);
			await result.save();
			return true;
		}
		return false;
	} catch (e) {
		return false;
	}
};

const all_users = async () => {
	try {
		const results = await Account.find({});
		return results.map((x) => x.username);
	} catch (e) {
		console.error(e);
		return [];
	}
};

const clear = async () => {
	await Account.deleteMany({});
};

module.exports = {
	signup: signup,
	auth: auth,
	get_username: get_username,
	username_exists: username_exists,
	all_users: all_users,
	delete_account: delete_account,
};

// tests
const run = async () => {
	console.log(await Account.find({}));
	//console.log(await auth("something", "attempt"));
	//console.log(await signup("juni", "junikimm717@gmail.com", "password"));
	//await clear();
};

//run();
